#!/usr/bin/env python3
from dotenv import load_dotenv #pip --user install dotenv,sudo pacman -S python-dotenv
from os import getenv
import json
import paho.mqtt.client as mqtt
import time
import threading
from http.server import HTTPServer
from ServerRaumHTTPHandler import ServerRaumHTTPHandler

class SensorClient:
    def __init__(self, serverIp, serverPort, brokerIp, brokerPort, room, interval, temp, humidity, tempLimit, humidLimit, startServer=True):
        self.serverIp = serverIp
        self.serverPort = serverPort
        self.room = room
        self.brokerIp = brokerIp
        self.brokerPort = brokerPort
        self.interval = interval
        self.temp = temp
        self.humidity = humidity
        self.tempLimit = tempLimit
        self.humidLimit = humidLimit
        
        self.mqtt_client = mqtt.Client(client_id="ServerClientRoom" + self.room)
        self.mqtt_client.connect(self.brokerIp, self.brokerPort)

        self.topiclimits = "ServerRoomCentral/limits/" + str(self.room)
        self.topicconfirm = "ServerRoomCentral/confirm/" + str(self.room)
        self.mqtt_client.subscribe(self.topiclimits)
        self.mqtt_client.subscribe(self.topicconfirm)

        self.mqtt_client.loop_start()
        self.mqtt_client.on_message = self.messageReceived

        self.hasCalledAlarm = False
        self.hasConfirmedAlarm = False

        self.startServer = startServer

    def messageReceived(self, client, userdata, message):
        data = message.payload.decode("utf-8")
        print(message.topic + ": " + data)
        # data kann nun wie ein normaler String verarbeitet werden
        # welches Topic wurde empfangen?
        if message.topic == self.topicconfirm:
            self.hasConfirmedAlarm = True
            pass
        if message.topic == self.topiclimits:
            limits = data.split(":")
            if len(limits) == 2:
                self.saveLimits(float(limits[0]), float(limits[1]))

    def readDataFromSensor(self):
        self.temp = min(self.temp + 0.5, 90.0)
        self.humidity = min(self.humidity + 0.5, 70.0)

        if (self.temp >= self.tempLimit or self.humidity >= self.humidLimit) and not self.hasConfirmedAlarm:
            self.mqtt_client.publish("sensorclient/alarm", self.room).wait_for_publish()
            self.hasCalledAlarm = True

        pass

    def statusInfo(self):
        return ':'.join([self.room, str(self.temp), str(self.humidity), str(self.tempLimit), str(self.humidLimit)])

    def run(self):
        if self.startServer:
            testserver = HTTPServer(("localhost", 1111), ServerRaumHTTPHandler)
            threading.Thread(target=testserver.serve_forever).start()
            print("Testserver started http://%s:%s" % ("localhost", "1111"))

        print("--- Starting publishing ---")
        while True:
            self.readDataFromSensor()
            self.mqtt_client.publish("sensorclient/data", self.statusInfo()).wait_for_publish()
            ServerRaumHTTPHandler.room = self.room
            ServerRaumHTTPHandler.temp = self.temp
            ServerRaumHTTPHandler.humid = self.humidity
            ServerRaumHTTPHandler.hlimit = self.humidLimit
            ServerRaumHTTPHandler.tlimit = self.tempLimit
            time.sleep(self.interval)

    def saveLimits(self, tempLimit, humidLimit):
        self.tempLimit = tempLimit
        self.humidLimit = humidLimit

        with open("limits.json", "w", encoding='utf-8') as write_file:
            json.dump({'temp_limit': self.tempLimit, 'humid_limit': self.humidLimit}, write_file)

    def prettyPrint(self):
        print("Server-IP: {}, Server-Port: {}, Room: {}, Broker-IP: {}, Interval: {}, Temperature: {}, Humidity: {}, Temperature-Limit: {}, Humidity-Limit: {}".format(
            self.serverIp,
            self.serverPort,
            self.room,
            self.brokerIp,
            self.interval,
            self.temp,
            self.humidity,
            self.tempLimit,
            self.humidLimit
            ))

# .env statt config.txt damit man auch Umgebungsvariablen verwenden kann
load_dotenv()
serverIp = getenv("SERVER_IP")
serverPort = getenv("SERVER_PORT")
brokerIp = getenv("BROKER_IP")
brokerPort = getenv("BROKER_PORT", "1883") # Standard: Unverschlüsselt
room = getenv("ROOM_ID")
tempLimit = getenv("TEMP_LIMIT", "40")
humidLimit = getenv("HUMID_LIMIT", "85")

try:
    with open('limits.json') as limitsJson:
        data = json.load(limitsJson)
        if "temp_limit" in data:
            tempLimit = data["temp_limit"]
    
        if "humid_limit" in data:
            humidLimit = data["humid_limit"]
except IOError:
    print("limits.json could not be read")

if serverIp is None or serverPort is None or brokerIp is None or room is None:
    print("Fehler: SERVER_IP, SERVER_PORT, BROKER_IP und ROOM müssen als Umgebungsvariablen definiert sein (z.B. in der .env-Datei)")
    print("Da ein fataler Fehler aufgetrten ist wird der Client heruntergefahren")
    exit()

try:
    serverPort = int(serverPort)
    brokerPort = int(brokerPort)
    tempLimit = int(tempLimit)
    humidLimit = int(humidLimit)
except TypeError:
    print("SERVER_PORT, BROKER_PORT, TEMP_LIMIT und HUMID_LIMIT müssen Zahlen sein!")
    exit()

if __name__ == "__main__":
    sensorClient = SensorClient(serverIp, serverPort, brokerIp, brokerPort, room, 0.5, 20, 50.0, tempLimit, humidLimit)
    sensorClient.prettyPrint()
    sensorClient.saveLimits(50, 70)
    print(sensorClient.statusInfo())
    sensorClient.run()
