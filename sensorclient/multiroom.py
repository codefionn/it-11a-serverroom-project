#!/usr/bin/env python3

import threading
from dotenv import load_dotenv #pip --user install dotenv,sudo pacman -S python-dotenv
from os import getenv
from sensorclient import SensorClient

load_dotenv()
serverIp = getenv("SERVER_IP")
serverPort = getenv("SERVER_PORT")
brokerIp = getenv("BROKER_IP")
brokerPort = getenv("BROKER_PORT", "1883") # Standard: Unverschlüsselt

try:
    serverPort = int(serverPort)
    brokerPort = int(brokerPort)
    #tempLimit = int(tempLimit)
    #humidLimit = int(humidLimit)
except TypeError:
    print("SERVER_PORT, BROKER_PORT, TEMP_LIMIT und HUMID_LIMIT müssen Zahlen sein!")
    exit()

for i in range(1, 10):
    room = "Raum " + str(110 + i)
    sensorclient = SensorClient(serverIp, serverPort, brokerIp, brokerPort, room, 0.5, 20, 50.0, 60.0, 80.0, startServer=False)
    threading.Thread(target=sensorclient.run).start()

sensorclient = SensorClient(serverIp, serverPort, brokerIp, brokerPort, "Raum 200", 0.5, 20, 50.0, 60.0, 80.0)
sensorclient.run()
