body {
    background: darkgreen;
    color: white;
    font-size: 2rem;
    text-align: center;
    padding: 0.5rem;
}
h1 {
    font-size: 5rem;
}
#clock, #hlimit, #humid, #room, #temp, #tlimit {
    color: red;
    font-weight: bold;
}
span {
    margin: 0 0.5rem;
}
