# sensorclient

Written in python3.

## Run

MQTT starten:

```sh
docker-compose up
```

Im diesem Repository ist eine /.env-Datei mit den einstellbaren Variablen.

## Dependencies

* dotenv
* mqtt-paho

## TODO

* MQTT-Verschlüsselung per Public-Private Cryptography
