use std::time::Duration;

use uuid::Uuid;

fn create_conn_opts<S0: Into<String>, S1: Into<String>>(client_id: S0, host: S1, port: u16) -> mqtt::MqttOptions {
    let rdm_uid = Uuid::new_v4();
    let mut conn_opts = mqtt::MqttOptions::new(
        format!("{}-{}", client_id.into(), rdm_uid),
        host.into(),
        port
    );
    conn_opts.set_keep_alive(Duration::from_secs(5));
    conn_opts.set_clean_session(true);
    conn_opts.set_transport(mqtt::Transport::Tcp);

    return conn_opts;
}

pub fn connect<S0: Into<String>, S1: Into<String>>(client_id: S0, host: S1, port: u16) -> (mqtt::AsyncClient, mqtt::EventLoop) {
    let conn_opts = create_conn_opts(client_id, host, port);

    mqtt::AsyncClient::new(conn_opts, 10)
}

pub fn connect_sync<S0: Into<String>, S1: Into<String>>(client_id: S0, host: S1, port: u16) -> (mqtt::Client, mqtt::Connection) {
    let conn_opts = create_conn_opts(client_id, host, port);

    mqtt::Client::new(conn_opts, 10)
}

#[inline]
pub fn handle_mqtt_error(client_result: Result<(), rumqttc::ClientError>) {
    if let Err(err) = client_result {
        eprintln!("Error publishing: {}: {:?}", err.to_string(), err);
    }
}
