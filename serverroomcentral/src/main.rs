// On Windows platform, don't show a console when opening the app.
#![windows_subsystem = "windows"]

extern crate rumqttc as mqtt;
extern crate dotenv;
extern crate tokio;

#[macro_use]
extern crate druid;
use druid::widget::{Label, Flex, Align, CrossAxisAlignment, Slider, Split, Button, Controller};
use druid::{AppLauncher, WindowDesc, Widget, WidgetExt, LensExt, LocalizedString, Data, Lens, Env, Color, Insets, Selector, WidgetId, Menu};

const TIME_TO_RECONNECT: std::time::Duration = std::time::Duration::from_secs(5);

use std::env;
use std::str;

mod mqtt_client;

const VERTICAL_WIDGET_SPACING: f64 = 20.0;
const HORIZONTAL_WIDGET_SPACING: f64 = 20.0;

const CLIENT_ID: &'static str = "ServerRoomCentral";
const SUB_PATH_DATA: &'static str = "sensorclient/data";
const SUB_PATH_ALARM: &'static str = "sensorclient/alarm";

const DEFAULT_TEXT_COLOR: Color = Color::WHITE;
const ERROR_TEXT_COLOR: Color = Color::RED;

const SAVE: Selector = Selector::new("eu.codefionn.serverroomanager.slider.save");

const MIN_LIMIT: f64 = 20.0;

#[derive(Clone, Data, Lens, Debug)]
struct ServerRoom {
    active: bool,
    room_id: String,
    temp: f32,
    humid: f32,
    temp_limit: f64,
    // User-set values
    temp_limit_self: f64,
    humid_limit: f64,
    // User-set values
    humid_limit_self: f64,
    alarm: bool,
}

impl ServerRoom {
    fn set_temp_limit(&mut self, temp_limit: f64) {
        if self.temp_limit_self == self.temp_limit {
            self.temp_limit_self = temp_limit;
        }

        self.temp_limit = temp_limit;
    }

    fn set_humid_limit(&mut self, humid_limit: f64) {
        if self.humid_limit_self == self.humid_limit {
            self.humid_limit_self = humid_limit;
        }

        self.humid_limit = humid_limit;
    }
}

impl Default for ServerRoom {
    #[inline]
    fn default() -> Self {
        Self {
            active: false,
            room_id: String::new(),
            temp: 0.0,
            humid: 0.0,
            temp_limit: 0.0,
            temp_limit_self: 0.0,
            humid_limit: 0.0,
            humid_limit_self: 0.0,
            alarm: false
        }
    }
}

#[derive(Clone, Data, Lens, Debug)]
struct GlobalState {
    server_rooms: [ServerRoom; 12],
    #[data(same_fn = "PartialEq::eq")]
    last_ping: Option<chrono::DateTime<chrono::Utc>>,
    status_message: String
}

impl Default for GlobalState {
    #[inline]
    fn default() -> Self {
        Self {
            server_rooms: Default::default(),
            last_ping: None,
            status_message: String::new()
        }
    }
}

struct DelegateMqttEvents {
    event_sink: druid::ExtEventSink
}

impl druid::AppDelegate<GlobalState> for DelegateMqttEvents {
    fn window_added(&mut self, _: druid::WindowId, _: &mut GlobalState, _: &Env, _: &mut druid::DelegateCtx) {
        let event_sink = self.event_sink.clone();
        tokio::task::spawn(async move {
            mqtt_notify_handler(event_sink).await
        });       
    }
}

fn failed_to_subscribe_to_mqtt_broker(data: &mut GlobalState) {
    const SECS: u64 = TIME_TO_RECONNECT.as_secs();
    data.status_message = format!("Failed to subscribe to MQTT-Broker. Reconnecting in {} seconds", SECS);
}

fn set_room(room_id: String, rooms: &mut [ServerRoom], setter: impl Fn(&mut ServerRoom)) -> bool {
    for room in rooms.iter_mut() {
        if room.active && room.room_id == room_id {
            setter(room);
            println!("{:?}", room);
            return true;
        }
    }

    for room in rooms.iter_mut() {
        if !room.active {
            room.room_id = room_id;
            room.active = true;
            setter(room);
            println!("{:?}", room);
            return true;
        }
    }

    return false;
}

async fn mqtt_notify_handler(event_sink: druid::ExtEventSink) {
    let (client, mut events) = create_mqtt_client();
    if let Err(_) = client.subscribe(SUB_PATH_DATA, mqtt::QoS::AtMostOnce).await {
        event_sink.add_idle_callback(failed_to_subscribe_to_mqtt_broker);
        return;
    }
    if let Err(_) = client.subscribe(SUB_PATH_ALARM, mqtt::QoS::AtMostOnce).await {
        event_sink.add_idle_callback(failed_to_subscribe_to_mqtt_broker);
        return;
    }

    let mut connected = false;

    loop {
        let notification = match events.poll().await {
            Ok(notification) => {
                if !connected {
                    event_sink.add_idle_callback(|state: &mut GlobalState| {
                        state.status_message = "Connected to MQTT".to_string();
                    });

                    connected = true;
                }

                notification
            },
            Err(_) => {
                event_sink.add_idle_callback(failed_to_subscribe_to_mqtt_broker);
                connected = false;
                // Wait to reconnect
                tokio::time::sleep(TIME_TO_RECONNECT).await;
                continue;
            }
        };

        match notification {
            mqtt::Event::Incoming(pkg) => {
                println!("Event-Incoming: {:?}", pkg);

                match pkg {
                    mqtt::Packet::PingResp => {
                        event_sink.add_idle_callback(move |data: &mut GlobalState| {
                            data.last_ping = Some(chrono::Utc::now())
                        });
                    },
                    mqtt::Packet::Publish(publish) => {
                        match publish.topic.as_str() {
                            SUB_PATH_DATA => {
                                mqtt_handle_subscribe_data(&event_sink, publish);
                            },
                            SUB_PATH_ALARM => {
                                mqtt_handle_subscribe_alarm(&event_sink, publish);
                            },
                            _ => {}
                        }
                    },
                    _ => {}
                }
            },
            mqtt::Event::Outgoing(pkg) => {
                println!("Event-Outgoing: {:?}", pkg);
            }
        }
    }
}

fn send_one_time<S: Into<String>, T: Into<Vec<u8>>>(topic: S, data: T) {
    let topic = topic.into();
    let data = data.into();
    std::thread::spawn(move || {
        let (mut client, mut conn) = create_mqtt_client_sync();
        client.subscribe(topic.clone(), mqtt::QoS::AtMostOnce).expect("Subscription failed");
        mqtt_client::handle_mqtt_error(client.publish(topic, mqtt::QoS::AtLeastOnce, false, data));
        client.disconnect().ok();
        for (_, notification) in conn.iter().enumerate() {
            if notification.is_err() {
                break;
            }
        }
    });
}

fn mqtt_confirm_alarm(state: &mut GlobalState, room_id: usize) {
    println!("Confirming alarm Room {}", room_id);

    let server_room = &mut state.server_rooms[room_id];
    server_room.alarm = false;
    send_one_time(format!("ServerRoomCentral/confirm/{}", server_room.room_id), format!(""));
}

fn mqtt_save_limits(state: &GlobalState, room_id: usize) {
    println!("Saving Limits of Room {}", room_id);

    let server_room = &state.server_rooms[room_id];
    send_one_time(format!("ServerRoomCentral/limits/{}", server_room.room_id), format!("{}:{}", server_room.temp_limit_self, server_room.humid_limit_self));
}

/// Bearbeitet die Daten die vom sensorclient gesendet werden
fn mqtt_handle_subscribe_data(event_sink: &druid::ExtEventSink, publish: mqtt::Publish) {
    let utf8_data = str::from_utf8(&publish.payload[..]).expect("Except UTF-8 String").to_string().clone();
    println!("Event-Incoming-Subscription: {}: {}", SUB_PATH_DATA, utf8_data);

    let data_points: Vec<&str> = utf8_data.splitn(5, r":").collect();
    if data_points.len() == 5 {
        if let (room_id, Ok(temp), Ok(humid), Ok(temp_limit), Ok(humid_limit))
                = (data_points[0].to_string().clone(), data_points[1].parse::<f32>(), data_points[2].parse::<f32>(), data_points[3].parse::<f64>(), data_points[4].parse::<f64>()) {
            event_sink.add_idle_callback(move |data: &mut GlobalState| {
                set_room(room_id, &mut data.server_rooms, move |server_room: &mut ServerRoom| {
                    server_room.temp = temp;
                    server_room.humid = humid;
                    server_room.set_temp_limit(temp_limit);
                    server_room.set_humid_limit(humid_limit);
                });
            });
        }
    }
    else {
        println!("Invalid {} event data", SUB_PATH_DATA);
    }

    event_sink.add_idle_callback(move |data: &mut GlobalState| {
        data.status_message = format!("Event-Incoming-Subscription: {}: {}", SUB_PATH_DATA, utf8_data);
    });
}

/// Bearbeitet den Alarm, der vom sensorclient gesendet wird
fn mqtt_handle_subscribe_alarm(event_sink: &druid::ExtEventSink, publish: mqtt::Publish) {
    let utf8_data = str::from_utf8(&publish.payload[..]).expect("Except UTF-8 String").to_string().clone();
    println!("Event-Incoming-Subscription: {}: {}", SUB_PATH_ALARM, utf8_data);

    event_sink.add_idle_callback(move |data: &mut GlobalState| {
        set_room(utf8_data.clone(), &mut data.server_rooms, move |server_room: &mut ServerRoom| {
            server_room.alarm = true;
        });
        data.status_message = format!("Event-Incoming-Subscription: {}: {}", SUB_PATH_ALARM, utf8_data);
    });
}

#[tokio::main]
async fn main() {
    dotenv::dotenv().ok();

    let main_window = WindowDesc::new(build_root_widget())
        .title("ServerRoomCentral")
        .window_size((880.0, 660.0));

    let launcher = AppLauncher::with_window(main_window);
    let event_sink = launcher.get_external_handle();
    let delegate_mqtt = DelegateMqttEvents { event_sink };
    let launcher = launcher.delegate(delegate_mqtt);

    launcher.launch(GlobalState::default()).expect("Failed to launch application");
}

fn create_mqtt_client() -> (mqtt::AsyncClient, mqtt::EventLoop) {
    let mqtt_server = env::var("MQTT_SERVER").expect("Excepted MQTT_SERVER env variable");
    let mqtt_port = env::var("MQTT_PORT").expect("Excepted MQTT_PORT env variable").parse::<u16>().expect("MQTT_PORT must be an integer");

    println!("Connecting to MQTT-Server {}:{}", mqtt_server, mqtt_port);

    return mqtt_client::connect(CLIENT_ID, mqtt_server, mqtt_port);
}

fn create_mqtt_client_sync() -> (mqtt::Client, mqtt::Connection) {
    let mqtt_server = env::var("MQTT_SERVER").expect("Excepted MQTT_SERVER env variable");
    let mqtt_port = env::var("MQTT_PORT").expect("Excepted MQTT_PORT env variable").parse::<u16>().expect("MQTT_PORT must be an integer");

    println!("Connecting to MQTT-Server {}:{}", mqtt_server, mqtt_port);

    return mqtt_client::connect_sync(CLIENT_ID, mqtt_server, mqtt_port);
}

macro_rules! named {
    ($a: literal, $b: expr) => {
        Flex::column()
            .with_child(Label::new($a))
            .with_child($b)
    };
    ($a: literal, $b: expr, $str: literal, $i: ident, $part: expr) => {
        {
            let i = $i.clone();
            Flex::column()
                .with_child(Label::new($a))
                .with_child(Flex::row()
                    .with_child($b)
                    .with_child(Label::new(move |data: &GlobalState, _env: &_| {
                        let server_room = &mut data.server_rooms.get(i).unwrap();
                        let result: String = $part(server_room);
                        format!($str, result)
                    }))
                )
        }
    };
}


struct SendSaveLimits {
    room_id: usize
}

fn build_root_widget() -> impl Widget<GlobalState> {
    let rooms = (0..3).collect::<Vec<usize>>().iter().map(|x| {
        let x = x.clone();
        (0..4).collect::<Vec<usize>>().iter().map(move |y| {
            let j = x * 4 + y;
            let widget_id = WidgetId::reserved(j as u16);
            let i = j.clone();
            let label = Label::new(move |data: &GlobalState, _env: &_| {
                let server_room = &mut data.server_rooms.get(i).unwrap();
                format!("{}", server_room.room_id)
            })
            .env_scope(move |env, data| {
                let server_room = &mut data.server_rooms.get(i).unwrap();
                env.set(druid::theme::TEXT_COLOR,
                        if server_room.alarm {
                            ERROR_TEXT_COLOR
                        }
                        else {
                            DEFAULT_TEXT_COLOR
                        });
            });

            let i = j.clone();
            let temp_label = Label::new(move |data: &GlobalState, _env: &Env| {
                    format!("Temperature: {} °C", data.server_rooms.get(i).unwrap().temp)
                })
                .env_scope(move |env, data| {
                    let server_room = &mut data.server_rooms.get(i).unwrap();
                    env.set(druid::theme::TEXT_COLOR,
                            if server_room.temp as f64 >= server_room.temp_limit {
                                ERROR_TEXT_COLOR
                            }
                            else {
                                DEFAULT_TEXT_COLOR
                            });
                });
            let i = j.clone();
            let humid_label = Label::new(move |data: &GlobalState, _env: &Env|
                                   format!("Humidity: {} %", data.server_rooms.get(i).unwrap().humid))
                .env_scope(move |env, data| {
                    let server_room = &mut data.server_rooms.get(i).unwrap();
                    env.set(druid::theme::TEXT_COLOR,
                            if server_room.humid as f64 >= server_room.humid_limit {
                                ERROR_TEXT_COLOR
                            }
                            else {
                                DEFAULT_TEXT_COLOR
                            });
                });

            let i = j.clone();
            let humid_limit_lens = GlobalState::server_rooms.as_ref().index(i).then(ServerRoom::humid_limit_self);
            let humid_limit_box = Slider::new()
                .with_range(MIN_LIMIT, 100.0)
                .lens(humid_limit_lens)
                //.controller(SendHumidMaxCtrl { room_id: i.clone() })
                .with_id(widget_id)
                // Could cause bugs if the client doesn't work right
                .disabled_if(move |data, _|
                             data.server_rooms.get(i).unwrap().humid_limit_self < MIN_LIMIT);
                //.env_scope(move |_env, data| {
                //    let server_room = &mut data.server_rooms.get(i).unwrap();
                //    send_one_time(format!("send/serverraum/{}/humid_limit", server_room.id), server_room.humid_limit.to_string().into_bytes());
                //});
            let humid_limit_box = named!("Max. Humid.", humid_limit_box, "{} %", i, |server_room: &ServerRoom| {
                let result = if server_room.humid_limit.round() == server_room.humid_limit_self.round() {
                    format!("{:.0}", server_room.humid_limit)
                } else {
                    format!("{:.0} -> {:.0}", server_room.humid_limit, server_room.humid_limit_self)
                };

                result
            });

            let i = j.clone();
            let temp_limit_lens = GlobalState::server_rooms.as_ref().index(i).then(ServerRoom::temp_limit_self);
            let temp_limit_box = Slider::new()
                .with_range(MIN_LIMIT, 100.0)
                .lens(temp_limit_lens)
                //.controller(SendTempMaxCtrl { room_id: i.clone() })
                .with_id(widget_id)
                // Could cause bugs if the client doesn't work right
                .disabled_if(move |data, _|
                             data.server_rooms.get(i).unwrap().temp_limit_self < MIN_LIMIT);
                //.env_scope(move |_env, data| {
                //    let server_room = &mut data.server_rooms.get(i).unwrap();
                //    send_one_time(format!("send/serverraum/{}/temp_limit", server_room.id), server_room.temp_limit.to_string().into_bytes());
                //});
            let temp_limit_box = named!("Max. Temp.", temp_limit_box, "{} °C", i, |server_room: &ServerRoom| {
                let result = if server_room.temp_limit.round() == server_room.temp_limit_self.round() {
                    format!("{:.0}", server_room.temp_limit)
                } else {
                    format!("{:.0} -> {:.0}", server_room.temp_limit, server_room.temp_limit_self)
                };

                result
            });

            let i = j.clone();
            let result = Flex::column()
                .with_child(label)
                .with_child(temp_label)
                .with_spacer(5.0)
                .with_child(humid_label)
                .with_child(temp_limit_box)
                .with_child(humid_limit_box)
                .with_child(Flex::row()
                            .with_child(Button::new("Save")
                                .on_click(move |ctx, data, _env| {
                                    mqtt_save_limits(data, i)
                            }))
                            .with_child(Button::new("Confirm alarm")
                                .on_click(move |ctx, data, _env| {
                                    mqtt_confirm_alarm(data, i);
                                })
                                .disabled_if(move |data, _| {
                                    !data.server_rooms.get(i).unwrap().alarm
                                }))
                )
                .cross_axis_alignment(CrossAxisAlignment::Start)
                .fix_size(185.0, 160.0)
                .padding(5.0);

            let i = j.clone();
            let result = result.disabled_if(move |data, _|
                                            !data.server_rooms.get(i).unwrap().active
                                            || (data.server_rooms.get(i).unwrap().temp <= 0.01
                                                && data.server_rooms.get(i).unwrap().humid <= 0.01));

            result
                .border(Color::WHITE, 0.5)
                .rounded(10.0)
        }).collect()
    }).collect::<Vec<Vec<druid::widget::Container<GlobalState>>>>();

    let label = Label::new("Server-Room-Manager");

    let mut layout = Flex::column()
        .with_child(label)
        .with_spacer(VERTICAL_WIDGET_SPACING);

    for rooms_row in rooms {
        let mut row = Flex::row();
        for room in rooms_row {
            row = row.with_child(room.padding(Insets::from(5.0)))
                .with_spacer(HORIZONTAL_WIDGET_SPACING);
        }

        layout = layout
            .with_child(row)
            .with_spacer(VERTICAL_WIDGET_SPACING);
    }


    let status_message_label = Label::new(|data: &GlobalState, _env: &Env| {
        if data.status_message.is_empty() {
            String::new()
        }
        else {
            format!("Last message: {}", data.status_message)
        }
    });

    let last_ping_label = Label::new(|data: &GlobalState, _env: &Env| {
            if let Some(datetime) = data.last_ping {
                let local_datetime: chrono::DateTime<chrono::Local> = chrono::DateTime::from(datetime);
                format!("Last ping: {}", local_datetime.format("%Y-%m-%d %H:%M:%S"))
            }
            else {
                String::new()
            }
        })
        .with_text_alignment(druid::TextAlignment::End);

    let statusbar = Split::columns(
            Align::left(status_message_label),
            Align::right(last_ping_label)
        )
        .split_point(0.7)
        .expand_width();

    Flex::column()
        .with_child(Align::centered(layout))
        .with_flex_spacer(f64::MAX)
        .with_child(statusbar)
}
