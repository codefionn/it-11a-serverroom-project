import React, {useEffect, useState, useRef} from 'react';
import './App.css';

const api_url = "http://localhost:1111/api";

interface RoomState {
  room_id: string,
  temp: number,
  humid: number,
  temp_limit: number,
  humid_limit: number
}

interface AppState {
  room: RoomState|null,
  loading: boolean,
  error: boolean,
  last_update: Date
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>ServerRoomClient - Web</h1>
      </header>
      <main>
        <Room />
      </main>
    </div>
  );
}

function fetchRoom(setState: React.Dispatch<React.SetStateAction<AppState>>) {
  fetch(api_url)
      .then(response => response.json())
      .then(response => setState({
          room: response,
          loading: false,
          error: false,
          last_update: new Date()
      }))
      .catch(error => {
        setState({
          room: null,
          loading: false,
          error: true,
          last_update: new Date()
        });

        console.log(error);
      })
}

function lpad(str: string, padding: string, size: number) {
  if (str.length >= size) {
    return str;
  }
  
  var result = str;
  while (result.length < size) {
    result = padding + result;
  }

  return result;
}

interface CurrentTimeProps {
  time: Date
}

function CurrentTime(props: CurrentTimeProps) {
  let hours = props.time.getHours().toString();
  let minutes = lpad(props.time.getMinutes().toString(), '0', 2);
  let seconds = lpad(props.time.getSeconds().toString(), '0', 2);
  return (
    <>
      {hours}:{minutes}:{seconds}
    </>
  )
}

const useInterval = (callback: () => void, delay: number) => {
  const savedCallback = useRef<() => void>();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);


  useEffect(() => {
    function tick() {
      if (savedCallback.current) {
        savedCallback.current();
      }
    }

    if (savedCallback.current) {
      savedCallback.current();
    }

    if (delay !== null) {
      const id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}

function Room() {
  let [state, setState] = useState<AppState>({
    room: null,
    loading: true,
    error: false,
    last_update: new Date()
  });

  useInterval(() => fetchRoom(setState), 5000);
  
  return (
    <>
      {state.loading && <div>Loading...</div>}
      {!state.loading && state.room &&
        <div>
          <h2>{state.room.room_id}</h2>
          <p>Aktuell ist es: <CurrentTime time={state.last_update}/></p>
          <p>Temperatur: {state.room.temp} ° Celsius </p>
          <p>Feuchtigkeit: {state.room.humid}%</p>
          <br/>
          <p>Die Limits für diese Raum betragen {state.room.temp_limit} ° Celsius und {state.room.humid_limit}% Luftfeuchtigkeit</p>
        </div>}
      {state.error && <div>An error occured</div>}
    </>
  );
}

export default App;
