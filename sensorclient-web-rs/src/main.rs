extern crate yew;
extern crate chrono;
#[macro_use]
extern crate log;
extern crate wasm_logger;
extern crate wasm_bindgen;
extern crate wasm_bindgen_futures;
extern crate load_dotenv;

mod app;

use app::App;

fn main() {
    wasm_logger::init(wasm_logger::Config::default());

    yew::start_app::<App>();
}
