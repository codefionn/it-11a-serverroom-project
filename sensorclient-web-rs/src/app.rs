use yew::prelude::*;
use chrono::prelude::*;
use serde::{Deserialize, Serialize, de::DeserializeOwned};
use web_sys::{Request, RequestInit, RequestMode, Response};
use wasm_bindgen_futures::JsFuture;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use load_dotenv::load_dotenv;

load_dotenv!();

#[function_component(App)]
pub fn app() -> Html {
    html! {
        <div>
            <header>
                <h1>{"ServerRoomClient - Web - Rust"}</h1>
            </header>
            <main>
                <Room />
            </main>
        </div>
    }
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
struct RoomState {
    room_id: String,
    temp: f32,
    humid: f32,
    temp_limit: f32,
    humid_limit: f32
}

#[derive(Debug, Clone, PartialEq)]
pub struct FetchError {
    err: JsValue,
}

impl std::fmt::Display for FetchError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.err, f)
    }
}

impl std::error::Error for FetchError {}

impl From<JsValue> for FetchError {
    fn from(value: JsValue) -> Self {
        Self { err: value }
    }
}

/// Make CORS GET request. Requires the response to be JSON. The JSON's format
/// is described by the type T.
pub async fn fetch_get<T: DeserializeOwned>(url: &str) -> Result<T, FetchError> {
    // Create our request
    let mut opts = RequestInit::new();
    opts.method("GET")
        .mode(RequestMode::Cors); // Remember to allow CORS on server!

    let request = Request::new_with_str_and_init(url, &opts)?;
    request.headers().set("Content-Type", "application/json")?;
    request.headers().set("Accept", "application/json")?;
    
    // Send the request
    let window = gloo::utils::window();
    let resp_value = JsFuture::from(window.fetch_with_request(&request)).await?;
    let resp: Response = resp_value.dyn_into().unwrap();

    // Parse the response
    let text = JsFuture::from(resp.text()?).await?;
    let text = text.as_string().unwrap();
    let text = text.as_str();
    log::info!("{}", text);

    if let Ok(json) = serde_json::from_str(text) {
        Ok(json)
    }
    else {
        Err(FetchError::from(JsValue::from("Error interpreting response as json")))
    }
}

/// Fetches data from Python client. Implement the following ServerRaumHTTPHandler:
///
/// ```python
/// if self.path == "/api":
///     self.send_response(200)
///     self.send_header("Content-Type", "application/json")
///     self.send_header("Access-Control-Allow-Origin", "*")
///     self.end_headers()
///     self.wfile.write(json.dumps(
///         {
///             "room_id": ServerRaumHTTPHandler.room,
///             "temp": ServerRaumHTTPHandler.temp,
///             "humid": ServerRaumHTTPHandler.humid,
///             "temp_limit": ServerRaumHTTPHandler.tlimit,
///             "humid_limit": ServerRaumHTTPHandler.hlimit
///         }).encode('utf-8'))
/// ```
async fn fetch_room(state: UseStateHandle<AppState>) {
    if let Ok(resp) = fetch_get(env!("API_URL")).await {
        state.set(AppState {
            room: Some(resp),
                loading: false,
                error: false,
                last_update: Utc::now()
            });
    }
    else {
        error!("Failed reading from {}", env!("API_URL"));
        state.set(AppState {
            room: None,
            loading: false,
            error: true,
            last_update: Utc::now()
        });
    }
}

#[derive(Clone, PartialEq, Debug)]
struct AppState {
    room: Option<RoomState>,
    loading: bool,
    error: bool,
    last_update: DateTime<Utc>
}

#[derive(Properties, PartialEq, Clone)]
struct CurrentTimeProps {
    time: DateTime<Utc>
}

fn lpad(mystr: String, delimiter: &str, size: usize) -> String {
    let len = mystr.len();
    if len >= size {
        return mystr;
    }

    return delimiter.repeat(size - len) + mystr.as_str();
}

#[function_component(CurrentTime)]
fn current_time(props: &CurrentTimeProps) -> Html {
    let local_time: DateTime<Local> = props.time.into();
    html! {
        <>{local_time.hour()}{":"}{lpad(local_time.minute().to_string(), "0", 2)}{":"}{lpad(local_time.second().to_string(), "0", 2)}</>
    }
}

/// The main content of the page
#[function_component(Room)]
fn room() -> Html {
    let state: UseStateHandle<AppState> = use_state(|| AppState {
        room: None,
        loading: true,
        error: false,
        last_update: Utc::now()
    });

    let state_copy = state.clone();
    use_effect(move || {
        use gloo::timers::callback::Timeout;

        // When loading => Don't wait, when already loaded => reload after 2 seconds
        let timeout = if (*state_copy).loading {
            wasm_bindgen_futures::spawn_local(fetch_room(state_copy));
            None
        }
        else {
            Some(Timeout::new(2000, move || {
                wasm_bindgen_futures::spawn_local(fetch_room(state_copy));
            }))
        };

        return move || {
            if let Some(timeout) = timeout {
                timeout.forget();
            }
        };
    });

    html! {
        <>
            if state.loading {
                <div>{"Loading..."}</div>
            }

            if !state.loading { if let Some(room) = &(*state).room {
              <div>
                <h2>{room.room_id.clone()}</h2>
                <p>{"Aktuell ist es: "} <CurrentTime time={state.last_update}/></p>
                <p>{"Temperatur: "} {room.temp} {" ° Celsius "}</p>
                <p>{"Feuchtigkeit: "} {room.humid} {"%"}</p>
                <br/>
                <p>{"Die Limits für diese Raum betragen "} {room.temp_limit} {" ° Celsius und "} {room.humid_limit} {" % Luftfeuchtigkeit"}</p>
              </div>
            }}

            if state.error {
                <div>{"An error occured"}</div>
            }
        </>
    }
}
